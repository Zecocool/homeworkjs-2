//Запитання 1
//Існують такі типи даних:
//Boolean - логічний (булевий) тип даних: true і false
//Number числа цілі, наприклад: 3 або з плаваючою крапкою наприклад: 1.6
//String - рядок, символьний тип даних, тобто послідовність символів.
//Symbol - примітивний тип даних (новий тип даних який появився в специфікації ES6)
//Спеціальний тип даних:
//undefined - зміна якій не присвоїли значення.
//null - нульове значення, "пусте значення"

// Запитання 2
//Різниця між == та === полягає у тому, що оператор === порівнює на рівність, а == на ідентичність.

//Запитання 3
//Оператор – це внутрішня функція JavaScript, тобто інструкція даної мови програмування,
//якою задається певний крок процесу обробки інформації. Оператори буають 3 типів:
// Унарні - вимагає один оператор і встановлюється перед або після нього;
// Бінарні - вимагають двох операторів і встановлюється між ними;
//Тернарні - Вимагають 3-х операторів; (Тернарний оператор є аналогом умовного оператора If...Else.)

let userName;
let userAge;

while (!userName || !userAge || userAge <= 0 || typeof userAge === "string") {
  userName = prompt("Enter your name:", userName);
  userAge = +prompt("Enter your age:", userAge);
}

if (userAge < 18) {
  alert("You are not allowed to visit this website");
} else if (userAge >= 18 && userAge < 22) {
  let agre = confirm("Are you sure you want to continue?");
  if (agre === true) {
    alert(`Welcome ${userName}`);
  } else if (agre === false) {
    alert("You are not allowed to visit this website");
  }
} else if (userAge > 22) {
  alert("Welcome " + userName);
}
